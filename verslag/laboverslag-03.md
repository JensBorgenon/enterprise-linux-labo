## Laboverslag: Labo 2

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Procedures

1. DNS-server opzetten met BIND
2. Forward lookup van alle hosts
3. Reverse lookup van alle hosts
4.MX-record met preference number 10
5. Slave-server opzetten

### Testplan en -rapport

- VM met BIND
- Forward lookups slagen
- Reverse lookups slagen
- Alias lookups slagen
- NS en MX record slagen
- alle acceptatietests succesvol

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Forward lookups opzetten
- Reverse lookups opzetten
- Alias lookups opzetten

#### Wat ging niet goed?

NS en MX records waren iets moeilijker, maar lukten ook.

#### Wat heb je geleerd?

- DNS-server opzetten met master en slave
- lookups configuren 

#### Waar heb je nog problemen mee?

- /

### Referenties

- https://github.com/bertvv
