## Laboverslag: Labo 2

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Procedures

1. Rollen httpd, mariadb en wordpress toegevoegd.
2. Map host_vars aangemaakt met pu004.yml in met de juiste variabelen.
3. Firewall en SELinux geconfigureerd
4. Een self-signed certificaat gemaakt

### Testplan en -rapport

- VM met Apache, PHP en MariaDB
- Firewall- en SELinux-instellingen
- Wordpress installatiepagina
- self-signed certificate
- acceptatietests succesvol

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- rollen installeren
- Firewall configureren
- Wordpress

#### Wat ging niet goed?

self-signed certificaat maken, dit omdat ik altijd met pu004.crs kreeg in plaat van .crt

#### Wat heb je geleerd?

- hoe je roles toevoegd
- hoe je host_vars gebruikt
- self-signed certificate maken 

#### Waar heb je nog problemen mee?

- Automatiseren van de self-signed certificate

### Referenties

- https://github.com/bertvv
- https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/ch-Web_Servers.html