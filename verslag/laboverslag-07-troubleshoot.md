## Laboverslag: Labo 7: Troubleshooten van een samba server

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Verschillende stappen

1. firewall samba doorlaten
2. bob en alice toevoegen aan groep foxtrot
3. smb.conf aanpassen: 	
 - workgroup aanpassen (geen invloed op testen)
 - share alpha: valid users = bob toevoegen
 - share bravo: sudo chcon -R -t samba share t bravo
			type van de share stond op public content rw t in plaats van samba share t
 - share charlie: directory mode = 0774 gezet
 - share delta : directory mode = 555 toevoegen
 - share echo: chmod /srv/shares 774 echo
 - share foxtrot: write list =  @foxtrot in plaats van valid users
4. service nmb starten zodat netbios name en workgroup ingeladen kan worden


### Resultaten van de test

Alle acceptance tests werken. Verbinden met samba server vanop hostsysteem werkt via IP-adres, niet via \\\\brokensamba\\.

### Wat ging er goed

- smb.conf aanpassen en configureren.

### Wat ging er minder goed

- time management
- gericht troubleshooten (veel gezocht in smb.conf, terwijl service niet draaide -> zoeken in logfiles)

#### Informatiebronnen

- http://www.slideshare.net/bertvanvreckem/linux-troubleshooting-tips
- https://access.redhat.com/documentation/en/red-hat-enterprise-linux/
- http://forums.fedoraforum.org/showthread.php?t=300078
- http://linuxcommand.org/man_pages/chcon1.html
