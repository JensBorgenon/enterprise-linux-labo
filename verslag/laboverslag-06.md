## Laboverslag: Labo 6

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Procedures

1. lease-times goed zetten
2. domain name en domain name server instellen
3. dhcp_subnets instellen met de juiste ranges
4. de gereserveerde host instellen met het juiste ip- en MAC-adres.

### Testplan en -rapport

- Manueel een nieuwe VM (Fedora) maken en twee netwerkadapters instellen zodat ze zijn aangesloten met het netwerk.
- Noteer van een interface het MAC-adres en zet het in de config-file bij de gereserveerde hosts.
- Boot de VM en controleer dat de ene interface het gereserveerde adres krijgt en de andere interface een adres uit een andere pool krijgt. 

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- lease-times goed zetten
- dhcp_subnets instellen met de juiste ranges
- de gereserveerde host instellen met het juiste ip- en MAC-adres.


#### Wat ging niet goed?

- domain name instellen, deze verwachte een andere syntax dan ik hanteerde. 

#### Wat heb je geleerd?

- hoe je een dhcp kan configureren met verschillende pools en gereserveerde hosts.

#### Waar heb je nog problemen mee?

- /

### Referenties

- https://github.com/bertvv


