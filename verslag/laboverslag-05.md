## Laboverslag: Labo 5

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Procedures

1. el7 user groups maken
2. el7 users aanmaken en aan de juiste groups toekennen
3. samba users aanmaken
4. samba shares maken en juiste valid users en write list zetten.
5. vsftpd-rol zoeken en aanpassen om zo in onze systeem te passen.


### Testplan en -rapport

- Het labo-verslag is aanwezig en volledig
- werkende samba met correcte leestoegang
- beschikbaar vanop hostsysteem
- werkende Vsftp-server met correcte leestoegang

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- user groups aanmaken
- users aanmaken
- samba users aanmaken
- configuratie rol vsftpd aanpassen

#### Wat ging niet goed?

- samba shares maken
- testen samba shares doen slagen (probleem met vm -> destroy en opnieuw opzetten)
- share beheer aanpassen zodat publieke gebruikers niet kunnen lezen en schrijven

#### Wat heb je geleerd?

- Hoe je een samba omgeving opzet en op een goede manier koppelt met lokale users.
- Hoe je de config van een rol aanpast en deze op jouw systeem afstelt.
- Hoe je een vsftp opzet zodat deze bereikbaar is vanop je hostsysteem.

#### Waar heb je nog problemen mee?

- /

### Referenties

- https://github.com/bertvv
- https://github.com/weareinteractive/ansible-vsftpd

