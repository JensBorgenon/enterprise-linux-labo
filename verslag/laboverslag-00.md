## Laboverslag: TITEL

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Procedures

1. Fork gemaakt van bertvv en deze gekloond lokaal.
2. README.md aangepast 
3. Vagrant en Ansible geïnstalleerd
4. Rollen toegekend aan de hosts door scriptje te runnen.
5. Hosts geconfigureerd door all.yml in te vullen.
6. Testen uitgevoerd

### Testplan en -rapport

- vagrant status
- vagrant up pu004
- acceptatietests succesvol

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- installatie vagrant en ansible
- rollen toekennen

#### Wat ging niet goed?

Hosts configureren, dit omdat ik nog niet vertrouwd was met de syntax van het opstellen van all.yml.

#### Wat heb je geleerd?

- Hoe BitBucket effectief werkt.
- Hoe vagrant en ansible werken.
- Hoe je scriptjes runt.
- Hoe je ssh keys maakt en toepast
- Hoe je all.yml opstelt.
- Hoe je je passwoorden hasht.

#### Waar heb je nog problemen mee?

- Geen 

### Referenties

- https://github.com/bertvv
- https://www.mkpasswd.net/index.php