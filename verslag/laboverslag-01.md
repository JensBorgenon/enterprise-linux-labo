## Laboverslag: Taak 01 - Actualiteit

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Cheat sheet

Mijn cheat-sheet is te vinden op mijn repo onder de map doc.

### Bijdrage aan een open source project

####Toelichting van de bijdrage

Als bijdrage heb ik gekozen om de handleiding OpsSchool aan te vullen met mijn kennis. Over het commando sed is er nog niets geïmplementeerd, dit neem ik voor mijn rekening. 

Verder is er ook nog niets ingevuld voor samba. Dit hebben we geleerd tijdens de lessen Enterprise Linux. Het is dus vanzelfsprekend dat ik mijn kennis wil delen hierover.

#### Toelichting van de gekozen aanpak: welke stappen heb je ondernomen?

- Opzoeken hoe commando sed werkt in de man-pages.
- Algemene info zoeken over samba
- samba rol bestuderen

#### Gebruikte bronnen

- http://www.opsschool.org/en/latest/
- http://linux.die.net/man/1/sed
- https://www.samba.org/samba/
- https://galaxy.ansible.com/detail#/role/3118

