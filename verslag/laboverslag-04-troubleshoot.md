## Laboverslag: Labo 4: Troubleshooting

- Naam cursist: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

### Verschillende stappen

- netwerk adapter inschakelen (ifup enp0s8)
- adressen verkeerd in /var/named/2.0.192.in-addr.arpa (overal na elke .com een punt vergeten)
- aanpassen van listen-on port 53 {127.0.0.01;192.168.56.2/24;}; in /etc/named.conf
- zone aanpassen naar 56.168.192.in-addr.arpa in /etc/named.conf
- reverse lookup zones aanpassen
- zone cynalco.com ip adressen en reverse lookup-adressen aanpassen in /var/named/cynalco.com
- NS lookup toevoegen in /var/named/cynalco.com

### Resultaten van de test

Alle acceptance tests werken en ik was de student die het het snelst heeft klaargespeeld.

### Wat ging er goed

- online info opzoeken over bepaalde errors
- samenwerkend vermogen van de groep

### Wat ging er minder goed

- time management
- gericht troubleshooten (veel tijd verspeeld bij onbelangrijke dingen)
- public domain doen werken (veel gezocht of services wel werkten, maar het was een fout in het config-bestand)

#### Informatiebronnen

- http://www.slideshare.net/bertvanvreckem/linux-troubleshooting-tips
- https://access.redhat.com/documentation/en/red-hat-enterprise-linux/
- http://www.cyberciti.biz/faq/howto-linux-unix-zone-file-validity-checking/
