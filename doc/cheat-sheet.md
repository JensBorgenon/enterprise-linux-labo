## Cheat sheet en checklists

- Naam student: Jens Borgenon
- Bitbucket repo: https://bitbucket.org/JensBorgenon/enterprise-linux-labo

Vul dit document zelf aan met commando's die je nuttig vindt, of waarvan je merkt dat je het verschillende keren moeten opzoeken hebt. Idem voor checklists voor troubleshooting, m.a.w. procedures die je kan volgen om bepaalde problemen te identificeren en op te lossen. Voeg waar je wil secties toe om de structuur van het document overzichtelijk te houden. Werk dit bij gedurende heel het semester!

Je vindt hier alvast een voorbeeldje om je op weg te zetten. Zie [https://github.com/bertvv/cheat-sheets](https://github.com/bertvv/cheat-sheets) voor een verder uitgewerkt voorbeeld.

### Commando's

| Taak                   | Commando |
| :---                   | :---     |
| IP-adress(en) opvragen | `ip a`   |

### Git workflow

Eenvoudige workflow voor een éénmansproject

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Huidige toestand project                    | `git status`              |
| Bestanden toevoegen/klaarzetten voor commit | `git add FILE...` (--all) |
| Bestanden "committen"                       | `git commit -m 'MESSAGE'` |
| Synchroniseren naar Bitbucket               | `git push`                |
| Wijzigingen van Bitbucket halen             | `git pull`                |

### Checklist netwerkconfiguratie

1. Is het IP-adres correct? `ip a`
2. Is de router/default gateway correct ingesteld? `ip r -n`
3. Is er een DNS-server? `cat /etc/resolv.conf`

### vagrant commando's

- vagrant status
- vagrant up (pu004)
- vagrant destroy (pu004)
- vagrant provision
- vagrant ssh pu004

### andere commando's

- ssh jens@192.0.2.50 --> zien of ssh connectie werkt met user jens
- named-checkconf <filepath> --> syntax controleren
- systemctl status named --> check if service named is running

###DNS



| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Bekijken welk ip adres site heeft               | `dig google.com`              |
| Bekijen welk domain name bij bepaalt ip past | `dig -x 209.132.183.81` |


###Samba


| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Kijken welke shares er zijn op de fileserver files             | `smbclient -L files`              |

###Firewall

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Kijken wat firewall doorlaat              | `sudo firewall-cmd --list-all`              |
| dns toevoegen aan de firewall settings| `sudo firewall-cmd --add-service:dns` |

###users aanpassen

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| add user to a group             | `usermod -g friends <Account>`              |
| add user to a group without loosing the ones he is already in| `usermod -aG friends bob` |
| add to current group the right to modify someFile.txt            | `chmod g+w someFile.txt`              |

### Network configuration

| Action                             | Command                                       |
| :---                               | :---                                          |
| List interfaces (and IP addresses) | `ip address`, `ip a`                          |
| Route table                        | `ip route`, `ip r`                            |
| DNS servers                        | `cat /etc/resolv.conf`                        |
| Set IP address of an interface*    | `ip address add 192.168.56.1/24 dev vboxnet0` |

### Managing services with `systemctl`

| Action                                      | Command                                          |
| :---                                        | :---                                             |
| List services                               | `systemctl list-units --type service`            |
| Query SERVICE status                        | `sudo systemctl status SERVICE.service`          |
| List failed services on boot                | `sudo systemctl --failed`                        |
| Start SERVICE                               | `sudo systemctl start SERVICE.service`           |
| Stop SERVICE                                | `sudo systemctl stop SERVICE.service`            |
| Restart SERVICE                             | `sudo systemctl restart SERVICE.service`         |
| *Kill* SERVICE (all processes) with SIGTERM | `sudo systemctl kill SERVICE.service`            |
| *Kill* SERVICE (all processes) with SIGKILL | `sudo systemctl kill -s SIGKILL SERVICE.service` |
| Start SERVICE on boot                       | `sudo systemctl enable SERVICE.service`          |
| Don't start SERVICE on boot                 | `sudo systemctl disable SERVICE.service`         |